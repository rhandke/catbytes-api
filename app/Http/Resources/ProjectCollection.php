<?php

declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProjectCollection extends ResourceCollection
{
    /** @return mixed[] */
    public function toArray($request): array
    {
        return [
            'count' => $this->count(),
            'data' => $this->collection,
        ];
    }
}
