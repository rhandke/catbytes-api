<?php

declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NonProfitResource extends JsonResource
{
    /** @return mixed[] */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'verified' => $this->verified,
            'projects' => $this->projects,
        ];
    }
}
