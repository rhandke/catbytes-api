<?php

declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectResource extends JsonResource
{
    /** @return mixed[] */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'specifications' => $this->specifications,
            'status' => $this->status,
            'nonProfit' => $this->nonProfit,
        ];
    }
}
