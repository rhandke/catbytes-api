<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Resources\NonProfitCollection;
use App\Http\Resources\NonProfitResource;
use App\Models\NonProfit;
use Illuminate\Http\Request;

class NonProfitController extends Controller
{
    /** @return NonProfitCollection<NonProfit> */
    public function index(): NonProfitCollection
    {
        return new NonProfitCollection(NonProfit::all());
    }

    public function store(Request $request): NonProfitResource
    {
        $data = $this->validateData($request);

        $nonProfit = NonProfit::create($data);

        return new NonProfitResource($nonProfit);
    }

    public function show(NonProfit $nonProfit): NonProfitResource
    {
        return new NonProfitResource($nonProfit);
    }

    public function update(Request $request, NonProfit $nonProfit): NonProfitResource
    {
        $data = $this->validateData($request);

        $nonProfit->name = $data['name'];
        $nonProfit->description = $data['description'];
        $nonProfit->save();

        return new NonProfitResource($nonProfit);
    }

    public function destroy(NonProfit $nonProfit): NonProfitResource
    {
        $nonProfit->delete();

        return new NonProfitResource($nonProfit);
    }

    /** @return mixed[] */
    protected function validateData(Request $request): array
    {
        return $request->validate([
            'name' => ['bail', 'required', 'string', 'min:2'],
            'description' => ['bail', 'required', 'string', 'min:2'],
        ]);
    }
}
