<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Resources\ProjectCollection;
use App\Http\Resources\ProjectResource;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ProjectController extends Controller
{
    /** @return ProjectCollection<Project> */
    public function index(): ProjectCollection
    {
        return new ProjectCollection(Project::all());
    }

    public function store(Request $request): ProjectResource
    {
        $data = $this->getValidatedData($request);

        return new ProjectResource(Project::create($data));
    }

    public function show(Project $project): ProjectResource
    {
        return new ProjectResource($project);
    }

    public function update(Request $request, Project $project): ProjectResource
    {
        $data = $this->getValidatedData($request);

        $project->name = $data['name'];
        $project->description = $data['description'];
        $project->specifications = $data['specifications'];
        $project->status = $data['status'];
        $project->non_profit_id = $data['non_profit_id'];
        $project->save();

        return new ProjectResource($project);
    }

    public function destroy(Project $project): ProjectResource
    {
        $project->delete();

        return new ProjectResource($project);
    }

    /** @return mixed[] */
    protected function getValidatedData(Request $request): array
    {
        return $request->validate([
            'name' => ['bail', 'required', 'string', 'min:2'],
            'description' => ['bail', 'required', 'string', 'min:2'],
            'specifications' => ['present'],
            'status' => ['bail', 'required', 'string', Rule::in(['open', 'development', 'closed'])],
            'non_profit_id' => ['bail', 'integer', 'min:1'],
        ]);
    }
}
