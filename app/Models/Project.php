<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Project extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'description', 'specifications', 'status', 'non_profit_id'];

    public function nonProfit(): BelongsTo
    {
        return $this->belongsTo(NonProfit::class);
    }
}
