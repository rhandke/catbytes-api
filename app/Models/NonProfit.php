<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class NonProfit extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'description', 'verified'];

    public function projects(): HasMany
    {
        return $this->hasMany(Project::class);
    }
}
