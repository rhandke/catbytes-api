<?php

namespace Tests\Feature;

use Database\Seeders\NonProfitsTableSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class NonProfitsTest extends TestCase
{
    use RefreshDatabase;

    public function testIndex(): void
    {
        $this->seed(NonProfitsTableSeeder::class);

        $this
            ->json('GET', '/non-profits', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'data' => [],
                'count' => 10,
            ]);
    }

    public function testRead(): void
    {
        $this->seed(NonProfitsTableSeeder::class);
        $id = 1;

        $this
            ->json('GET', "/non-profits/{$id}", ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'id' => $id,
                ],
            ]);
    }

    public function testCreate(): void
    {
        $this->seed(NonProfitsTableSeeder::class);
        $data = [
            'name' => 'Generic Non Profit Organisation',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'verified' => false,
        ];

        $this
            ->json('POST', '/non-profits', $data, ['Accept' => 'application/json'])
            ->assertStatus(201)
            ->assertJson([
                'data' => $data,
            ]);
    }

    public function testUpdate(): void
    {
        $this->seed(NonProfitsTableSeeder::class);
        $data = [
            'name' => 'Generic Non Profit Organisation',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        ];

        $this
            ->json('Patch', '/non-profits/1', $data, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'data' => $data,
            ]);
    }

    public function testDestroy(): void
    {
        $this->seed(NonProfitsTableSeeder::class);
        $id = 10;
        $this
            ->json('DELETE', "/non-profits/{$id}", ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'id' => $id,
                ],
            ]);
    }
}
