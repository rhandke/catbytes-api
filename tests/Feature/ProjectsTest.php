<?php

namespace Tests\Feature;

use Database\Seeders\NonProfitsTableSeeder;
use Database\Seeders\ProjectsTableSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProjectsTest extends TestCase
{
    use RefreshDatabase;

    public function testIndex(): void
    {
        $this->seed(NonProfitsTableSeeder::class);
        $this->seed(ProjectsTableSeeder::class);

        $this
            ->json('GET', '/projects', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'data' => [],
            ]);
    }

    public function testRead(): void
    {
        $this->seed(NonProfitsTableSeeder::class);
        $this->seed(ProjectsTableSeeder::class);

        $id = 1;
        $this
            ->json('GET', "/projects/{$id}", ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'id' => $id,
                ],
            ]);
    }

    public function testCreate(): void
    {
        $this->seed(NonProfitsTableSeeder::class);
        $this->seed(ProjectsTableSeeder::class);

        $data = [
            'name' => 'Generic Project',
            'description' => 'Some generic project',
            'specifications' => null,
            'status' => 'open',
            'non_profit_id' => 1
        ];

        $this
            ->json('POST', '/projects', $data, ['Accept' => 'application/json'])
            ->assertStatus(201)
            ->assertJson([
                'data' => array_filter($data, function ($key) {
                    if ($key === 'non_profit_id') {
                        return false;
                    }

                    return true;
                }, ARRAY_FILTER_USE_KEY),
            ]);
    }

    public function testUpdate(): void
    {
        $this->seed(NonProfitsTableSeeder::class);
        $this->seed(ProjectsTableSeeder::class);

        $id = 1;
        $data = [
            'name' => 'Generic Project',
            'description' => 'Some generic project',
            'specifications' => null,
            'status' => 'open',
            'non_profit_id' => 1
        ];

        $this
            ->json('PATCH', "/projects/{$id}", $data, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'data' => array_merge(['id' => $id], array_filter($data, function ($key) {
                    if ($key === 'non_profit_id') {
                        return false;
                    }

                    return true;
                }, ARRAY_FILTER_USE_KEY)),
            ]);
    }

    public function testDestroy(): void
    {
        $this->seed(NonProfitsTableSeeder::class);
        $this->seed(ProjectsTableSeeder::class);

        $id = 1;
        $this
            ->json('DELETE', "/projects/{$id}", ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'id' => $id,
                ],
            ]);
    }
}
