<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Project;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class ProjectFactory extends Factory
{
    protected $model = Project::class;

    /** @return mixed[] */
    public function definition(): array
    {
        $statusOptions = [
            'open',
            'development',
            'closed',
        ];

        return [
            'name' => $this->faker->streetName,
            'description' => $this->faker->text,
            'specifications' => '[]',
            'status' => Arr::random($statusOptions),
        ];
    }
}
