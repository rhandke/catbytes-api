<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\NonProfit;
use Illuminate\Database\Eloquent\Factories\Factory;

class NonProfitFactory extends Factory
{
    protected $model = NonProfit::class;

    /** @return mixed[] */
    public function definition(): array
    {
        return [
            'name' => $this->faker->company,
            'description' => $this->faker->text,
            'verified' => $this->faker->boolean,
        ];
    }
}
