<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\NonProfit;
use Illuminate\Database\Seeder;

class NonProfitsTableSeeder extends Seeder
{
    public function run(): void
    {
        NonProfit::factory()->count(10)->create();
    }
}
