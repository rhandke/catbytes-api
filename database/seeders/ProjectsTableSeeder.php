<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\NonProfit;
use App\Models\Project;
use Illuminate\Database\Seeder;

class ProjectsTableSeeder extends Seeder
{
    public function run(): void
    {
        NonProfit::all()->each(function (NonProfit $nonProfit) {
            $projects = collect();

            for ($i = 0; $i < random_int(1, 20); $i++) {
                $project = Project::factory()->make();
                $projects->push($project);
            }

            $nonProfit->projects()->saveMany($projects);
        });
    }
}
