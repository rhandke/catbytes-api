<?php

declare(strict_types=1);

use App\Http\Controllers\NonProfitController;
use App\Http\Controllers\ProjectController;
use Illuminate\Support\Facades\Route;

Route::apiResources([
    'non-profits' => NonProfitController::class,
    'projects' => ProjectController::class,
]);
